       IDENTIFICATION DIVISION.
       PROGRAM-ID. BSTRING.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  BOOLEAN-BLANK    PIC X.
           88 IS-BLANK      VALUE "T".
           88 IS-NOT-BLANK  VALUE "F".
       01  S                PIC X(5) VALUE "ABCDE".
       PROCEDURE DIVISION.
      *    CHECK S IS SPACES
           IF S = SPACES
              SET IS-BLANK TO TRUE 
           ELSE
              SET IS-NOT-BLANK TO TRUE
           END-IF.
           DISPLAY BOOLEAN-BLANK
           GOBACK.